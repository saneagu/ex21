package ro.orangeStartIT;
import java.util.*;

public class Test {

    public static void main(String[] args) {
        List<Employee> employees = Arrays.asList(
                new Employee(22, 1001, "Jon"),
                new Employee(19, 1003, "Steve"),
                new Employee(23, 1005, "Kevin"),
                new Employee(20, 1010, "Ron"),
                new Employee(18, 1111, "Lucy"));
        //Five objects of type Employee where you will add the attributes
        System.out.println("Before Sorting the Employee data: ");
        for (Employee employee : employees) {
            System.out.println(employee);
        }//Print the employees before sorting - forEach

        Collections.sort(employees, (o1, o2) -> o1.getAge() - o2.getAge()); //Use lambda expression for sorting by age
        System.out.println("\nAfter Sorting the Employee data by Age: ");
        for (Employee employee : employees) {
            System.out.println(employee);
        }

        //Use lambda expression for sorting by name
        Collections.sort(employees, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        System.out.println("\nAfter Sorting the Employee data by Name: ");
        for (Employee employee : employees) {
            System.out.println(employee);
        }

        Collections.sort(employees, (o1, o2) -> o1.getId() - o2.getId()); //Use lambda expression for sorting by id
        System.out.println("\nAfter Sorting the Employee data by ID: ");
        for (Employee employee : employees) {
            System.out.println(employee);
        }
    }
}
