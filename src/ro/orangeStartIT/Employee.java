package ro.orangeStartIT;

public class Employee {
    private int age;
    private String name;
    private int id;

    public Employee(int age, int id, String name) {
        this.age = age;
        this.id = id;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String toString(){
        return "Age = " + this.age + ", Name = " + this.name + ", ID = " + this.id;
    }
}
